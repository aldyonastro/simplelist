import 'package:SimpleList/promodetail/promo_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
          child: _renderBody()
      ),
    );
  }

  Widget _renderBody() {
    final List<Tuple4<int, String, String, String>> promoList = [
      Tuple4(1, "Title 1", "End 29 Feb 2020", "https://picsum.photos/800/450?random=1"),
      Tuple4(2, "Title 2", "End 29 Feb 2020", "https://picsum.photos/800/450?random=2"),
      Tuple4(3, "Title 3", "End 29 Feb 2020", "https://picsum.photos/800/450?random=3"),
      Tuple4(4, "Title 4", "End 29 Feb 2020", "https://picsum.photos/800/450?random=4"),
      Tuple4(5, "Title 5", "End 29 Feb 2020", "https://picsum.photos/800/450?random=5")
    ];

    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 8,),
            Text(
              'Promotions :',
              style: Theme.of(context).textTheme.headline5,
            ),
            SizedBox(height: 8,),
            ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: promoList.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () => { navigateToDetail(promoList[index]) },
                    child: _renderItemList(promoList[index], index),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    height: 16,
                    color: Colors.transparent,
                  );
                })
          ],
        ),
      ),
    );
  }

  Widget _renderItemList(Tuple4<int, String, String, String> promoData, int index) {
    int id = promoData.item1;
    String title = promoData.item2;
    String endDate = promoData.item3;
    String imgUrl = promoData.item4;

    return Column(
      children: [
        ClipRRect(//Make It Round for topLeft & topRight
          borderRadius: BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8)),
          child: Hero(
            tag: imgUrl+id.toString(),
            child: AspectRatio(//Apply Aspect Ratio
              aspectRatio: 16/9,
              child: CachedNetworkImage(
                placeholder: (context, url) {
                  return Center(
                      child: SizedBox(width: 32, height: 32,
                        child: CircularProgressIndicator(),
                      )
                  );
                },
                errorWidget: (context, url, error) => Icon(Icons.error_outline),
                imageUrl: imgUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        ClipRRect(//Make It Round for bottomLeft & bottomRight
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8), bottomRight: Radius.circular(8)),
          child: Container(
            color: Colors.black12,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Row(
              children: [
                Expanded(
                  flex: 9,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Hero(
                        tag: title+id.toString(),
                        child: Text(
                          title,
                          style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16,letterSpacing: 0.2,),
                        ),
                      ),
                      SizedBox(height: 4,),
                      Text(
                        endDate,
                        style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w300, fontSize: 12,letterSpacing: 0.2,),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Icon(
                      Icons.chevron_right,
                      color: Colors.black,
                      size: 32,
                    ))
              ],
            ),
          ),
        )
      ],
    );
  }

  //region Navigation
  void navigateToDetail(Tuple4<int, String, String, String> param) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PromoDetail(promoData: param,)
        ),
    );
  }
  //endregion
}


