import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

class PromoDetail extends StatefulWidget {
  PromoDetail({
    this.key,
    @required this.promoData
  }) : super(key: key);

  final Key key;
  final Tuple4<int, String, String, String> promoData;

  @override
  _PromoDetailState createState() => _PromoDetailState();
}

class _PromoDetailState extends State<PromoDetail> {

  @override
  Widget build(BuildContext context) {
    String promoId = widget.promoData.item1.toString();
    String promoTitle = widget.promoData.item2;
    String promoImage = widget.promoData.item4;

    return Scaffold(
      appBar: AppBar(
        title: Text(promoTitle),
        centerTitle: false,
      ),
      body: Container(
        child: Hero(
          tag: promoImage+promoId,
          child: AspectRatio(
            aspectRatio: 16/9,
            child: CachedNetworkImage(
              placeholder: (context, url) => Container(color: Colors.grey),
              errorWidget: (context, url, error) => Icon(Icons.error_outline),
              imageUrl: promoImage,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}